<?php

namespace Drupal\user_clock\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Datetime\DrupalDateTime;
/**
 * Class UserClockPageForm.
 */
class UserClockPageForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_clock_page_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, UserInterface $user = NULL) {
    $form['clock_time'] = [
      '#type' => 'markup',
      '#markup' => '<div id="current-user-time">'.$this->getUserCurrentTime().'</div>',
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#ajax' => [
        'callback' => '::getUserCurrentTimeCallback',
        'event' => 'click',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t("wait please.."),
        ],
      ],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
//    foreach ($form_state->getValues() as $key => $value) {
//      drupal_set_message($key . ': ' . $value);
//    }

  }

  /**
   * {@inheritdoc}
   */
  public function getUserCurrentTimeCallback(array &$form, FormStateInterface $form_state) {
    $ajax_response = new AjaxResponse();
    $date = $this->getUserCurrentTime();
    $ajax_response->addCommand(new HtmlCommand('#current-user-time', $date));
    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function getUserCurrentTime() {
    $timezone = drupal_get_user_timezone();
    $dt = new \DateTime('now', new \DateTimezone($timezone));
    $dt =  DrupalDateTime::createFromDateTime($dt);
    return $dt->format('Y-m-d H:i:s');
  }
}
