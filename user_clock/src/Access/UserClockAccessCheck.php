<?php
namespace Drupal\user_clock\Access;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Checks access for displaying configuration translation page.
 */
class UserClockAccessCheck {

  /**
   * A custom access check.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   */
  public function access (AccountInterface $account) {
    $perm_user_profile = FALSE;
    $perm_all_user_pages = FALSE;
    $current_path = \Drupal::service('path.current')->getPath();

    $has_user_profile = $account->hasPermission('view on user profile');
    if ($has_user_profile) {
      preg_match('/^\/user\/\d+$/', $current_path, $matches);
      preg_match('/^\/user\/\d+\/clock-page$/', $current_path, $matches_clock_page);
      $perm_user_profile = !empty($matches[0])||!empty($matches_clock_page[0]);
    }
    $has_all_user_pages = $account->hasPermission('view on all user pages');
    if ($has_all_user_pages) {
      preg_match('/^\/user\/.+$/', $current_path, $matches);
      $perm_all_user_pages = !empty($matches[0]);
    }
    return AccessResult::allowedIf($perm_user_profile || $perm_all_user_pages);
  }
}