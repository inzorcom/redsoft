<?php

namespace Drupal\write_log\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class UserLoggerForm.
 */
class UserLoggerForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'user_logger_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config =  \Drupal::config('write_log.writelogconf');
    $account = \Drupal::currentUser();

    if ($account->id() === 0) {
      $form['user_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Name'),
        '#required' => TRUE,
      ];
    }
    else {
      $form['user_name'] = [
        '#type' => 'value',
        '#value' => $account->getAccountName(),
        '#required' => TRUE,
      ];
    }
    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#required' => TRUE,
    ];
    $add_info_conf = $config->get('add_info');
    if ($add_info_conf) {
      $form['telephone'] = [
        '#type' => 'tel',
        '#title' => $this->t('Telephone'),
        '#description' => $this->t('Telephone format: (999)999-9999'),
      ];
      $form['email'] = [
        '#type' => 'email',
        '#title' => $this->t('Email'),
      ];
    }
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $vals = $form_state->getValues();
    preg_match('/^\(\d{3}\)\d{3}-\d{4}$/', $vals['telephone'], $matches);
    if (empty($matches) && (!empty($vals['telephone']))) {
      $form_state->setErrorByName('telephone', t('Format of input is not correct. You need something like this: (999)999-9999'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $vals = $form_state->getValues();
    drupal_set_message('Thanks for your request');
    $message = "Username: " . $vals['user_name'] . ", Message: " . $vals['description'];
    \Drupal::logger('write_log')->notice($message);
  }

}
