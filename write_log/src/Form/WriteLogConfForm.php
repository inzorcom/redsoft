<?php

namespace Drupal\write_log\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class WriteLogConfForm.
 */
class WriteLogConfForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'write_log.writelogconf',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'write_log_conf_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('write_log.writelogconf');
    $form['add_info'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Anonymous can provide additional information'),
      '#description' => $this->t('telephone and email fields will be added to the logger form if this option will be checked'),
      '#default_value' => $config->get('add_info'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('write_log.writelogconf')
      ->set('add_info', $form_state->getValue('add_info'))
      ->save();
  }

}
